#!/usr/bin/env python
# File: pyqtplot21.py
# Name: D.Saravanan
# Date: 29/02/2024

""" Script to create plot using the GraphicsLayoutWidget in PyQtGraph """

import sys

import numpy as np
import pyqtgraph as pg
from PyQt6 import QtCore, QtWidgets

pg.setConfigOptions(antialias=True)


class MainWindow(QtWidgets.QMainWindow):
    """Subclass of QMainWindow to customize application's main window."""

    def __init__(self, yval):
        super().__init__()
        self.yval = yval

        # set the size parameters (width, height) pixels
        self.setFixedSize(QtCore.QSize(640, 480))

        # set the central widget of the window
        self.graphWidget = pg.GraphicsLayoutWidget(show=True)
        self.setCentralWidget(self.graphWidget)

        # set the background color using hex notation #121317 as string
        self.graphWidget.setBackground("#121317")

        # widget for generating figures
        self.graphLine = self.graphWidget.addPlot(title="Random Normal")

        # set the background grid for both the x and y axis
        self.graphLine.showGrid(x=True, y=True, alpha=0.5)

		# set the legend which represents given line
        self.graphLine.addLegend(offset=(-10, 10), labelTextSize="9pt")

        # set the axis labels (position and text), style parameters
        styles = {"color": "#dcdcdc", "font-size": "10pt"}
        self.graphLine.setLabel("left", "f(x)", **styles)
        self.graphLine.setLabel("bottom", "x", **styles)

        # set the axis limits within the specified ranges and padding
        self.graphLine.setXRange(0, self.yval.size, padding=0)
        self.graphLine.setYRange(self.yval.min(), self.yval.max(), padding=0.1)

        # plot data: y values with line drawn using Qt's QPen types
        self.graphLine.plot(
            self.yval,
            name="random normal data",
            pen=pg.mkPen("#f4d054", width=1, style=QtCore.Qt.PenStyle.SolidLine),
            symbol="+",
            symbolSize=8,
            symbolBrush=("#f7022a"),
            symbolPen=None,
        )


def main():
    """Need one (and only one) QApplication instance per application.
    Pass in sys.argv to allow command line arguments for the application.
    If no command line arguments than QApplication([]) is required."""
    app = QtWidgets.QApplication(sys.argv)

    yval = np.random.normal(size=100)

    window = MainWindow(yval)  # an instance of the class MainWindow
    window.show()  # windows are hidden by default

    sys.exit(app.exec())  # start the event loop


if __name__ == "__main__":
    main()

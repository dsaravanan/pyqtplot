#!/usr/bin/env python
# File: pyqtplot20.py
# Name: D.Saravanan
# Date: 29/02/2024

""" Script to create plot using the GraphicsLayoutWidget in PyQtGraph """

import sys

import numpy as np
import pyqtgraph as pg
from PyQt6 import QtCore, QtWidgets

pg.setConfigOptions(antialias=True)


class MainWindow(QtWidgets.QMainWindow):
    """Subclass of QMainWindow to customize application's main window."""

    def __init__(self, xval, wav1, wav2, wav3):
        super().__init__()
        self.xval = xval
        self.wav1 = wav1
        self.wav2 = wav2
        self.wav3 = wav3

        # set the size parameters (width, height) pixels
        self.setFixedSize(QtCore.QSize(640, 480))

        # set the central widget of the window
        self.graphWidget = pg.GraphicsLayoutWidget(show=True)
        self.setCentralWidget(self.graphWidget)

        # set the background color using hex notation #121317 as string
        self.graphWidget.setBackground("#121317")

        # widget for generating figures
        self.graphLine = self.graphWidget.addPlot(title="Sine wave")

        # set the background grid for both the x and y axis
        self.graphLine.showGrid(x=True, y=True, alpha=0.5)

        # set the legend which represents given line
        self.graphLine.addLegend(offset=(10, -10), labelTextSize="9pt")

        # set the axis labels (position and text), style parameters
        styles = {"color": "#dcdcdc", "font-size": "10pt"}
        self.graphLine.setLabel("left", "f(x)", **styles)
        self.graphLine.setLabel("bottom", "x", **styles)

        # graphPlot method call
        self.graphPlot(self.xval, self.wav1, self.wav2, self.wav3)

    def graphPlot(self, xval, wav1, wav2, wav3):
        """Method accepts x and y parameters to plot."""

        # set the axis limits within the specified ranges and padding
        self.graphLine.setXRange(xval[0], xval[-1], padding=0)
        self.graphLine.setYRange(wav1.min(), wav1.max(), padding=0.1)

        # plot data: y values with line drawn using Qt's QPen types
        self.graphLine.plot(xval, wav1, name="sin(x)", pen=pg.mkPen("#aefd6c"))
        self.graphLine.plot(xval, wav2, name="sin(x)&sup2;", pen=pg.mkPen("#ffd1df"))
        self.graphLine.plot(xval, wav3, name="&frac12; sin(x)", pen=pg.mkPen("#7bc8f6"))


def main():
    """Need one (and only one) QApplication instance per application.
    Pass in sys.argv to allow command line arguments for the application.
    If no command line arguments than QApplication([]) is required."""
    app = QtWidgets.QApplication(sys.argv)

    xval = np.linspace(-2 * np.pi, 2 * np.pi, 1000, retstep=False)

    # an instance of the class MainWindow
    window = MainWindow(xval, np.sin(xval), np.sin(xval) ** 2, np.sin(xval) / 2)
    window.show()  # windows are hidden by default

    sys.exit(app.exec())  # start the event loop


if __name__ == "__main__":
    main()
